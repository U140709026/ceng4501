package state;

public class TestGumballMachine {
	
	public static void main(String[] args){
		GumballMachine machine = new GumballMachine(2);
		
		machine.ejectQuarter();
		
		machine.insertQuarter();
		machine.turnCrank();
		
		machine.insertQuarter();
		machine.turnCrank();
		
		machine.insertQuarter();
		machine.turnCrank();
		
		
	}

}
