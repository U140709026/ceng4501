package state;

public class Sold implements State {

	GumballMachine machine;
	
	public Sold(GumballMachine machine) {
		this.machine = machine;
	}
	
	
	@Override
	public void insertQuarter() {
		System.out.println("Please wait. A gumball will be given.");
	}

	@Override
	public void ejectQuarter() {
		System.out.println("It's too late, gumball will be given");
	}

	@Override
	public void turnCrank() {
		System.out.println("Turning the crank twice does not give you another gumball");

	}

	@Override
	public void dispense() {
		machine.releaseBall();
		if (machine.getGbCount() == 0){
			machine.setState(machine.sold_out);
		}else{
			machine.setState(machine.no_quarter);
		}		
	}

}
