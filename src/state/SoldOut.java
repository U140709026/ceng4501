package state;

public class SoldOut implements State {

	GumballMachine machine;
	
	public SoldOut(GumballMachine machine) {
		this.machine = machine;
	}
	
	
	@Override
	public void insertQuarter() {
		System.out.println("You can not insert another quarter. The machine is sold out!");
	}

	@Override
	public void ejectQuarter() {
		System.out.println("You haven't inserted a quarter!");
	}

	@Override
	public void turnCrank() {
		System.out.println("You haven't inserted a quarter!");

	}

	@Override
	public void dispense() {
		System.out.println("No gumball dispensed!");

	}

}
