package state;

import java.util.Random;

public class HasQuarter implements State {

	Random random = new Random(System.currentTimeMillis());
	GumballMachine machine;
	
	public HasQuarter(GumballMachine machine) {
		this.machine = machine;
	}
	
	@Override
	public void insertQuarter() {
		System.out.println("You can not insert another quarter");
	}

	@Override
	public void ejectQuarter() {
		machine.setState(machine.no_quarter);
		System.out.println("Quarter returned");


	}

	@Override
	public void turnCrank() {
		int winner = random.nextInt(10);
		if(winner ==0 && machine.getGbCount() >1 ){
			machine.setState(machine.winner);
		}else{
			machine.setState(machine.sold);
		}

	}

	@Override
	public void dispense() {
		System.out.println("No gumball dispensed!");
	}

}
