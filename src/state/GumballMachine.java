package state;

public class GumballMachine {

	State sold_out;
	State no_quarter;
	State has_quarter;
	State sold;
	State winner;
	
	private State state;
	
	private int gbCount;
	
	public GumballMachine(int gbCount) {
		this.gbCount = gbCount;
		sold_out = new SoldOut(this);
		no_quarter = new NoQuarter(this);
		has_quarter = new HasQuarter(this);
		sold = new Sold(this);
		winner = new Winner(this);
		
		if (gbCount > 0){
			state = no_quarter;
		}
	}
	
	public void insertQuarter(){
		state.insertQuarter();
	}
	
	public void ejectQuarter(){
		state.ejectQuarter();
		
	}
	
	public void turnCrank(){
		state.turnCrank();
		dispense();
		
	}
	
	public void dispense(){
		state.dispense();

	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public int getGbCount() {
		return gbCount;
	}

	public void setGbCount(int gbCount) {
		this.gbCount = gbCount;
	}
	
	public void releaseBall(){
		System.out.println("Gumball is coming");
		gbCount--;
	}
	
}
