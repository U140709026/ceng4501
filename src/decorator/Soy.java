package decorator;

public class Soy extends CondimentDecorator {

	public Soy(Beverage decoratedBeverage) {
		super(decoratedBeverage);
	}
	
	@Override
	public float cost() {
		return (float) 0.6 + decoratedBeverage.cost();
	}

	@Override
	public String getDescription() {
		
		return decoratedBeverage.getDescription() + " Soy";
	}

}
