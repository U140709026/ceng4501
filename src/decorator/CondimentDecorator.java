package decorator;

public abstract class CondimentDecorator extends Beverage {

	Beverage decoratedBeverage;
	
	public CondimentDecorator(Beverage decoratedBeverage) {
		this.decoratedBeverage = decoratedBeverage;
	}
	
	public abstract String getDescription();
	
}
