package decorator;

public class DarkRoastwithMocha extends Beverage {

	public DarkRoastwithMocha() {
		description = "Dark Roast" + " + mocha";
	}
	@Override
	public float cost() {
		return (float) 4.5 + (float) 0.5;
	}

}
