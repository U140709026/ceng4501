package templatemethod;

public class Coffee extends CafeinBeverage{
	


	protected void addCondiments() {
		System.out.println("Adding Sugar and Milk");
		
	}

	protected void brew() {
		System.out.println("Brewing Coffee");
		
	}

}
