package strategy;

public class DuckSimulator {

	public static void main(String[] args) {
		Duck redHD = new RedHeadDuck();
		redHD.display();
		redHD.performFly();
		redHD.performQuack();
		redHD.swim();

		
		Duck malD = new MallardDuck();
		malD.display();
		malD.performFly();
		malD.performQuack();
		malD.swim();

		Duck rubD = new RubberDuck();
		rubD.display();
		rubD.performFly();
		rubD.performQuack();
		rubD.swim();
		
		Duck decD = new DecoyDuck();
		decD.display();
		decD.performFly();
		decD.performQuack();
		decD.setQuackBehaviour(new Squeak());
		decD.performQuack();
		decD.swim();
				
		Duck modelDuck = new ModelDuck();
		modelDuck.display();
		modelDuck.performFly();
		modelDuck.setFlyBehaviour(new FlyRocketPowered());
		modelDuck.performFly();
	}

}
