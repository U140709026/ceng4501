package iterator;

import java.util.ArrayList;
import java.util.LinkedList;

public class TestJavaIterator {

	public static void main(String[] args) {
		ArrayList<String> list = new ArrayList<>();
		list.add("a");
		list.add("b");
		
		java.util.Iterator itr = list.iterator();
		
		while(itr.hasNext()){
			System.out.println(itr.next());
		}

		LinkedList<String> list2 = new LinkedList<>();
		list2.add("x");
		list2.add("x");
		list2.add("x");
		list2.add("y");
		list2.add("x");
		
		java.util.Iterator itr2 = list2.iterator();
		
		while(itr2.hasNext()){
			String str = (String)itr2.next();
			if (str.equals("x"))
				itr2.remove();
		}
		
		itr2 = list2.iterator();
		while(itr2.hasNext()){
			System.out.println(itr2.next());
		}
		
		
		for (String str: list){
			System.out.println(str);
		}
	}

}
