package iterator;

import java.util.ArrayList;

public class PancakeHouseMenu implements Menu{

	private ArrayList menuItems;
	
	public PancakeHouseMenu() {
		menuItems = new ArrayList();
		addItem("K&G Breakfast", " Content K&G", true, 2.99);
		addItem("Regular Breakfast", " Content Regular", true, 1.99);
		addItem("Blueberry Breakfast", " Content Blueberry", true, 3.99);
	}
	
	public void addItem(String name, String description,
			boolean vegeterian, double price){
		MenuItem menuItem = new MenuItem(name, description, vegeterian, price);
		menuItems.add(menuItem);
	}


	
	public java.util.Iterator  createIterator(){
		
		return menuItems.iterator();
	}

	@Override
	public String getMenuDefinition() {
		// TODO Auto-generated method stub
		return "-------BREAKFAST--------";
	}
}
