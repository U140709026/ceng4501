package iterator;

import java.util.Hashtable;
import java.util.Iterator;

public class CafeMenu implements Menu{

	private Hashtable menuItems = new Hashtable();
	
	public CafeMenu(){
		addItem("A","A Desc",true, 5);
		addItem("B","B Desc",true, 4);
		addItem("C","C Desc",true, 3);
	}
	
	public void addItem(String name, String desc, boolean veg,
			double price){
		MenuItem item = new MenuItem(name, desc, veg, price);
		menuItems.put(name, item);
		
	}


	@Override
	public Iterator createIterator() {
		return menuItems.values().iterator();
	}

	@Override
	public String getMenuDefinition() {
		return "-------Cafe Menu------";
	}
	
	
}
