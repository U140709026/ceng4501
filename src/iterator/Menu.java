package iterator;

public interface Menu {

	 java.util.Iterator createIterator();
	 
	 String getMenuDefinition();
}
