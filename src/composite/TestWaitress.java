package composite;

public class TestWaitress {

	public static void main(String[] args) {
		MenuComponent rootMenu = new Menu("All Menus", " All Menus Desc");
		
		MenuComponent breMenu = new Menu("BreakFast", " BreakFast Desc");
		breMenu.add(new MenuItem("Egg", "Boiled egg", true, 2));
		breMenu.add(new MenuItem("Tomato", " Sliced Tomato", true, 1));
		
		rootMenu.add(breMenu);
		
		
		MenuComponent lunchMenu = new Menu("Lunch", " Lunch Desc");
		lunchMenu.add(new MenuItem("Soup", "Tomato soup", true, 3));
		lunchMenu.add(new MenuItem("Chicken", " chicken wings", true, 7));
		
		MenuComponent dessertMenu = new Menu("Dessert Menu", "Dessert Menu Desc");
		dessertMenu.add(new MenuItem("Puding", "puding desc", true, 2));
		dessertMenu.add(new MenuItem("Baklava", "baklava desc", true, 2));
		lunchMenu.add(dessertMenu);
		
		
		rootMenu.add(lunchMenu);
		
		
		Waitress waitress = new Waitress(rootMenu);
		waitress.print();
		
	}

}
