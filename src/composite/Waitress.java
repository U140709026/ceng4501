package composite;

public class Waitress {

	MenuComponent rootMenu;
	
	public Waitress(MenuComponent rootMenu) {
		this.rootMenu = rootMenu;
	}
	
	
	public void print(){
		rootMenu.print();
	}
}
