package composite;

import java.util.Iterator;

public interface MenuComponent {

	default public String getName() {
		throw new UnsupportedOperationException();
	}

	default public String getDescription() {
		throw new UnsupportedOperationException();
	}

	default public double getPrice() {
		throw new UnsupportedOperationException();
	}

	default public boolean isVegetarion() {
		throw new UnsupportedOperationException();
	}

	default void print() {
		throw new UnsupportedOperationException();
	}

	default void add(MenuComponent comp) {
		throw new UnsupportedOperationException();
	}

	default void remove(MenuComponent comp) {
		throw new UnsupportedOperationException();
	}

	default MenuComponent getChild(int index) {
		throw new UnsupportedOperationException();
	}

	Iterator createIterator(); 
}
