package composite;

import java.util.ArrayList;

public class Menu implements MenuComponent{
	
	
	ArrayList<MenuComponent> children = new ArrayList<>();
	String name;
	String description;
	
	
	
	public Menu( String name, String description) {
		super();
		this.children = children;
		this.name = name;
		this.description = description;
	}

	@Override
	public String getName(){
		return name;
	}

	@Override
	public String getDescription() {
		return description;
	}
	
	@Override
	public void add(MenuComponent comp){
		children.add(comp);
	}

	@Override
	public void remove(MenuComponent comp){
		children.add(comp);
	}
	
	@Override
	public MenuComponent getChild(int index){
		return children.get(index);
	}
	
	@Override
	public void print(){
		System.out.println(name);
		System.out.println(description);
		System.out.println("--------------");
		
		for(MenuComponent comp: children){
			comp.print();
		}
	}
	
}
