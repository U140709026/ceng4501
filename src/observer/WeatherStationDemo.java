package observer;

public class WeatherStationDemo {

	public static void main(String[] args) {
		WeatherData wd = new WeatherData();
		
		java.util.Observer disp1 = new CurrentConditionsDisplay();
		java.util.Observer disp2 = new StatisticsDisplay();
		java.util.Observer disp3 = new ForecastDisplay();
		//java.util.Observer disp4 = new FutureDisplay();
		
		wd.addObserver(disp1);
		wd.addObserver(disp2);
		wd.addObserver(disp3);
		
		wd.measurementsChanged();
		
	}

}
