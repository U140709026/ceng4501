package observer;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

public class WeatherData extends Observable{

	WeatherStation station  = new WeatherStation();
	
	 double getTemperature(){
		return station.currentTemp;
	}
	
	 double getHumidity(){
		return station.getCurrentHumidity();
	}
	
	 double getPressure(){
		return station.getCurrentPressure();
	}
	
	public void measurementsChanged() {
		setChanged();
		notifyObservers();
	}

	
	
	

	

	
}
