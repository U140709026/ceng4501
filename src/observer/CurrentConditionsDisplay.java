package observer;

import java.util.Observable;

public class CurrentConditionsDisplay implements java.util.Observer{

	

	@Override
	public void update(Observable o, Object arg) {
		// TODO Auto-generated method stub
		WeatherData wd = (WeatherData) o;
		System.out.println("CurrentConditionsDisplay " + wd.getHumidity() + " " + wd.getPressure() +" " + wd.getTemperature());

	}
}
