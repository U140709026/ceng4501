package factory;

public class NYStylePepperoniPizza extends Pizza {

	public NYStylePepperoniPizza(PizzaIngredientFactory factory) {
		super(factory);
	}
}
