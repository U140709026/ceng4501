package factory;

public class NYStyleCheesePizza extends Pizza {

	public NYStyleCheesePizza(PizzaIngredientFactory factory) {
		super(factory);
		description = "NY Style Cheese Pizza";
	}
	
}
