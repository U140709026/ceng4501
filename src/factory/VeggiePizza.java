package factory;

public class VeggiePizza extends Pizza {

	public VeggiePizza(PizzaIngredientFactory factory) {
		super(factory);
	}
}
