package factory;

public class NYStyleVeggiePizza extends Pizza {

	public NYStyleVeggiePizza(PizzaIngredientFactory factory) {
		super(factory);
	}
}
