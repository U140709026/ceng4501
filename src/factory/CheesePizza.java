package factory;

public class CheesePizza extends Pizza {

	public CheesePizza(PizzaIngredientFactory factory) {
		super(factory);
	}
}
