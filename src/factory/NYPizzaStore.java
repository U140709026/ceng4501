package factory;

public class NYPizzaStore extends PizzaStore {

	PizzaIngredientFactory factory = new NYPizzaIngredientFactory();
	
	public void serviceToCar(){
		
	}

	@Override
	Pizza createPizza(String type) {
		Pizza pizza = null;
		if (type.equals("cheese")) {
			pizza = new NYStyleCheesePizza(factory);
		} else if (type.equals("pepperoni")) {
			pizza = new NYStylePepperoniPizza(factory);
		} else if (type.equals("clam")) {
			pizza = new NYStyleClamPizza(factory);
		} else if (type.equals("veggie")) {
			pizza = new NYStyleVeggiePizza(factory);
		}
		return pizza;
	}

}
