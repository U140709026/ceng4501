package factory;

public class ChicagoPizzaIngredientFactory implements PizzaIngredientFactory {

	@Override
	public Dough createDough() {
		
		return new ThickCrustDough();
	}

	@Override
	public Sauce createSauce() {
		// TODO Auto-generated method stub
		return new Marinara();
	}

	@Override
	public Cheese createCheese() {
		// TODO Auto-generated method stub
		return new Regiano();
	}

}
